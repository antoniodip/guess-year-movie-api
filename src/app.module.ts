import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { ApolloServerPluginLandingPageLocalDefault } from 'apollo-server-core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MovieModule } from './modules/movie/movie.module';
import { PlayerModule } from './modules/player/player.module';

@Module({
    imports: [
        GraphQLModule.forRoot<ApolloDriverConfig>({
            driver: ApolloDriver,
            typePaths: ['./src/modules/**/*.graphql'],
            debug: false,
            playground: false,
            plugins: [ApolloServerPluginLandingPageLocalDefault()],
        }),
        MongooseModule.forRoot('mongodb+srv://admin:admin@cluster0.fj69ttk.mongodb.net/?retryWrites=true&w=majority',
            {
                dbName: 'guess-year-movie',
                useUnifiedTopology: true,
                useNewUrlParser: true,
                autoCreate: true
            }),
        MovieModule,
        PlayerModule
    ],
    controllers: [AppController],
    providers: [AppService]
})
export class AppModule {}

