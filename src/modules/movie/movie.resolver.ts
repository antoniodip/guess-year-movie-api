import { Query, Resolver } from "@nestjs/graphql";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { MoviesWithReleases } from "../../generator/graphql";
import { Movie, MovieDocument } from "./movie.schema";

@Resolver('Movie')
export class MovieResolver {

    constructor(@InjectModel(Movie.name) private readonly movieModel: Model<MovieDocument>) {}

    /**
     * retrieve all the movies
     */
    @Query('movies')
    async getAll(): Promise<Movie[]> {
        return this.movieModel.find();
    }

    /**
     * retrieve n random movies with the correct release year, plus two wrong release years
     */
    @Query('moviesWithReleases')
    async getMoviesWithReleases(root: any, args: { n: number }): Promise<MoviesWithReleases[]> {
        const movies = await this.movieModel.aggregate<Movie>([
            { $sample: { size: args.n } } // pick n random movies from DB
        ]);
        return movies.map(movie => { return { 
            movie,
            wrongYears: [(movie.releaseYear+1), (movie.releaseYear-1)]
        } });
    }

    // @Mutation()
    // async createCity(root: any, { city }: { city: InputCity }): Promise<City> {
    //     return new this.cityModel(city).save();
    // }

}