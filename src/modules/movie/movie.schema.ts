/* eslint-disable indent */
import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
@ObjectType()
export class Movie {

    @Prop({ required: true, unique: true })
    @Field(() => ID, { description: 'Movie id' })
    id: string;

    @Prop({ required: true, unique: true })
    @Field(() => String, { description: 'Movie name' })
    title: string;

    @Prop()
    @Field(() => Int, { description: 'Movie release year' })
    releaseYear: number;

}
export const MovieSchema = SchemaFactory.createForClass(Movie);
export type MovieDocument = Movie & Document;