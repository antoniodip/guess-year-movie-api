import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MovieResolver } from './movie.resolver';
import { Movie, MovieSchema } from './movie.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Movie.name, schema: MovieSchema }])
    ],
    providers: [MovieResolver]
})
export class MovieModule {}
