import { Mutation, Query, Resolver } from "@nestjs/graphql";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { PlayerScoresInput } from "../../generator/graphql";
import { Player, PlayerDocument } from "./player.schema";

@Resolver('Player')
export class PlayerResolver {

    constructor(@InjectModel(Player.name) private readonly playerModel: Model<PlayerDocument>) {}

    /**
     * retrieve all the players
     */
    @Query('players')
    async getAll(): Promise<Player[]> {
        console.log('Retrieving all the players');
        return this.playerModel.find();
    }

    /**
     * Retrieve high scores given a username
     */
    @Query('highScores')
    async getPlayerHighScores(root: any, args: { username: string }): Promise<number[]> {
        const player = await this.playerModel.findOne({ username: args.username });
        return player.highScores;
    }

    /**
     * Retrieve the leaders board
     */
    @Query('leadersBoard')
    async getLeadersBoard(): Promise<Player[]> {
        return [];
    }

    /**
     * Save player's high scores
     */
    @Mutation('saveScores')
    async saveScores(root: any, { data }: { data: PlayerScoresInput }): Promise<boolean> {
        try {
            console.log(`Finding player [${data.username}]`, { data });
            const player = await this.playerModel.findOne({ username: data.username });
        
            if (!player)
                // player does not exist, add it in DB with the related high scores
                await new this.playerModel({ username: data.username, highScores: data.scores }).save();
            else {
                // player already exists, so just add the new scores
                player.highScores.push(...data.scores);
                await player.save();
            }
        } catch (error) {
            console.error('Error trying to save player high scores', { error });
            return false;
        }

        return true;
    }

}