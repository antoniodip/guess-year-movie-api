/* eslint-disable indent */
import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
@ObjectType()
export class Player {

    @Prop({ required: true })
    @Field(() => String, { description: 'Player name' })
    username: string;

    @Prop()
    @Field(() => [Int], { description: 'Player high scores' })
    highScores: number[];

}
export const PlayerSchema = SchemaFactory.createForClass(Player);
export type PlayerDocument = Player & Document;