import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PlayerResolver } from './player.resolver';
import { Player, PlayerSchema } from './player.schema';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: Player.name, schema: PlayerSchema }])
    ],
    providers: [PlayerResolver]
})
export class PlayerModule {}
