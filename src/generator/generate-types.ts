import { GraphQLDefinitionsFactory } from "@nestjs/graphql";
import { join } from "path";

const definitionsFactory = new GraphQLDefinitionsFactory();
definitionsFactory.generate({
    typePaths: ['./src/modules/**/*.graphql'], // where to find graphql schemas
    path: join(process.cwd(), 'src/generator/graphql.ts'), // where to save auto-generated ts classes
    outputAs: 'class', // generate classes instead of interfaces
    watch: false, // automatically generate typings whenever any .graphql file changes
    emitTypenameField: false, // generate the additional __typename field for every object type
    skipResolverArgs: false // generate resolvers as plain fields without arguments
});