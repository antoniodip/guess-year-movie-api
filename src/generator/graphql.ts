
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class PlayerScoresInput {
    username: string;
    scores: number[];
}

export class Movie {
    id: string;
    title: string;
    releaseYear?: Nullable<number>;
}

export class MoviesWithReleases {
    movie: Movie;
    wrongYears?: Nullable<number[]>;
}

export abstract class IQuery {
    abstract movies(): Nullable<Movie[]> | Promise<Nullable<Movie[]>>;

    abstract moviesWithReleases(n: number): Nullable<MoviesWithReleases[]> | Promise<Nullable<MoviesWithReleases[]>>;

    abstract players(): Nullable<Player[]> | Promise<Nullable<Player[]>>;

    abstract highScores(username: string): Nullable<number[]> | Promise<Nullable<number[]>>;

    abstract leadersBoard(): Nullable<Player[]> | Promise<Nullable<Player[]>>;
}

export class Player {
    id: string;
    username: string;
    highScores?: Nullable<number[]>;
}

export abstract class IMutation {
    abstract saveScores(data?: Nullable<PlayerScoresInput>): Nullable<boolean> | Promise<Nullable<boolean>>;
}

type Nullable<T> = T | null;
