import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.useGlobalPipes(new ValidationPipe());

    const port = 9000;
    await app.listen(port, () => {
        console.log(`\n***** App started successfully *** Listening on port: ${port} *****\n`);
    });
}
bootstrap();
